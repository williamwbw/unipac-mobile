import 'package:flutter/material.dart';
import 'package:unipacapp/screens/login.dart';
import 'package:unipacapp/screens/home.dart';
import 'package:unipacapp/screens/company.dart';
import 'package:unipacapp/screens/operator.dart';
import 'package:unipacapp/screens/product.dart';
import 'package:unipacapp/screens/details.dart';
import 'package:unipacapp/screens/batch.dart';
import 'package:unipacapp/screens/machine_data_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
        primaryColor: Color(0xff3E8D80),
        accentColor: Color(0xffFFF9FC),
        backgroundColor: Color(0xffF7F9FC),
        scaffoldBackgroundColor: Color(0xffFFFFFF),
      ),
      initialRoute: LoginPage.id,
      routes: {
        LoginPage.id: (context) => LoginPage(),
        HomePage.id: (context) => HomePage(),
        CompanyPage.id: (context) => CompanyPage(),
        MachineDataPage.id: (context) => MachineDataPage(),
        OperatorPage.id: (context) => OperatorPage(),
        ProductPage.id: (context) => ProductPage(),
        DetailsPage.id: (context) => DetailsPage(),
        BatchPage.id: (context) => BatchPage(),
      },
    );
  }
}
//
//textButtonTheme: TextButtonThemeData(
//style: TextButton.styleFrom(
//primary: Color(0xff3E8D80),
//),
//),
