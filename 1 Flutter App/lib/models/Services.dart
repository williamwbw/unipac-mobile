import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'User.dart';

class Services {
  static Future getMachines() async {
    try {
      var url = Uri.parse(
          'http://118.107.242.221:8585/api/MachineInfo/GetAllMachineWCompany/');
      var response = await http.get(url);

      if (response.statusCode == 200) {
        List<Machine> list = parseMachines(response.body);
        return list;
      } else {
        print(response.statusCode);
        throw Exception('Error');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Machine> parseMachines(dynamic responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Machine>((json) => Machine.fromJson(json)).toList();
  }
}
