import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/fault.dart';

class FaultData extends ChangeNotifier {
  List<Fault> _fault = [
    Fault(
      faultSummary: 'Fault Summary',
      faultLog: 'Fault Log',
      date: '03/07/2021 16:04:45',
    ),
  ];

  UnmodifiableListView<Fault> get fault {
    return UnmodifiableListView(_fault);
  }

  int get faultCount {
    return _fault.length;
  }
}
