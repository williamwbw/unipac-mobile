import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/company.dart';

class CompanyData extends ChangeNotifier {
  List<Company> _companies = [
    Company(
      name: 'Unipac Sdn Bhd',
      email: 'unipac@gmail.com',
      address:
          '2, Jalan Jurubina U1/18, Hicom-glenmarie Industrial Park, 40150 Shah Alam, Selangor',
    ),
    Company(
      name: 'Secure 365',
      email: 'unipac@gmail.com',
      address:
          '2, Jalan Jurubina U1/18, Hicom-glenmarie Industrial Park, 40150 Shah Alam, Selangor',
    ),
    Company(
      name: 'Anonymous I trying for long text',
      email: 'unipac@gmail.com',
      address:
          '2, Jalan Jurubina U1/18, Hicom-glenmarie Industrial Park, 40150 Shah Alam, Selangor',
    ),
    Company(
      name: 'Yoho BOBO',
      email: 'unipac123@gmail.com',
      address:
          '2, Jalan Jurubina U1/18, Hicom-glenmarie Industrial Park, 40150 Shah Alam, Selangor',
    ),
  ];

  UnmodifiableListView<Company> get tasks {
    return UnmodifiableListView(_companies);
  }

  int get taskCount {
    return _companies.length;
  }

  void addTask(String newTaskTitle) {
    final task = Company(name: newTaskTitle);
    _companies.add(task);
    notifyListeners();
  }

  void updateTask(Company task) {
    task.toggleDone();
    notifyListeners();
  }

  void deleteTask(Company task) {
    _companies.remove(task);
    notifyListeners();
  }
}
