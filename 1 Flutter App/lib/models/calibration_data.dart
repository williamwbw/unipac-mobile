import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/calibration.dart';

class CalibrationData extends ChangeNotifier {
  List<Calibration> _calibrations = [
    Calibration(
      serialnumber: 'QMALSXCJD',
      date: '10/2/2021',
    ),
    Calibration(
      serialnumber: 'QMALSXCJD',
      date: '12/2/2021',
    ),
    Calibration(
      serialnumber: 'QMALSXCeeD',
      date: '19/2/2021',
    ),
    Calibration(
      serialnumber: 'QMALvssD',
      date: '18/2/2021',
    ),
    Calibration(
      serialnumber: 'QMALfsdfsdfs',
      date: '15/2/2021',
    ),
    Calibration(
      serialnumber: 'QMALBBSJD',
      date: '14/2/2021',
    ),
  ];

  UnmodifiableListView<Calibration> get calibration {
    return UnmodifiableListView(_calibrations);
  }

  int get calibrationCount {
    return _calibrations.length;
  }
}
