class Event {
  final String eventSummary;
  final String date;

  Event({
    this.eventSummary = '',
    this.date = '',
  });
}
