class Machine {
  Machine(
      {required this.id, required this.serialNum, required this.companyName});
  int id;
  String serialNum;
  String companyName;

  factory Machine.fromJson(Map<String, dynamic> json) {
    return Machine(
      id: json['Id'] as int,
      serialNum: json['SerialNumber'] as String,
      companyName: json['CompanyName'] as String,
    );
  }
}
