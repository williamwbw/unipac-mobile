class Fault {
  final String faultSummary;
  final String faultLog;
  final String date;

  Fault({
    this.faultSummary = '',
    this.faultLog = '',
    this.date = '',
  });
}
