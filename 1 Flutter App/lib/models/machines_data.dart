import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/machine.dart';

class MachineData extends ChangeNotifier {
  List<Machine> _machines = [
    Machine(
      id: 2,
      serialNum: 'QMU343422',
      status: 'production',
      companyName: 'Unipac Sdn Bhd',
    ),
    Machine(
      id: 3,
      serialNum: 'QMU343423',
      status: 'standby',
      companyName: 'Unipac Sdn Bhd',
    ),
    Machine(
      id: 3,
      serialNum: 'QMU343424',
      status: 'off',
      companyName: 'Unipac Sdn Bhd',
    ),
    Machine(
      id: 5,
      serialNum: 'QMU343425',
      status: 'production',
      companyName: 'testing',
    ),
  ];

  UnmodifiableListView<Machine> get machines {
    return UnmodifiableListView(_machines);
  }

  int get machineCount {
    return _machines.length;
  }

  int get machineActive {
    return _machines.where((i) => i.status == 'production').length;
  }

  int get machineStandby {
    return _machines.where((i) => i.status == 'standby').length;
  }

  int get machineOff {
    return _machines.where((i) => i.status == 'off').length;
  }
}
