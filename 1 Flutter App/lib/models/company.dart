class Company {
  final String name;
  final String email;
  final String address;
  bool isDone;

  Company(
      {this.name = 'Company Name',
      this.isDone = false,
      this.email = '@gmail.com',
      this.address = 'address'});

  void toggleDone() {
    isDone = !isDone;
  }
}
