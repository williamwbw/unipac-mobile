class Machine {
  Machine({
    required this.id,
    required this.serialNum,
    required this.companyName,
    this.status = 'off',
  });
  int id;
  String serialNum;
  String companyName;
  final String status;

  factory Machine.fromJson(Map<String, dynamic> json) {
    return Machine(
      id: json['Id'] as int,
      serialNum: json['SerialNumber'] as String,
      companyName: json['CompanyName'] as String,
    );
  }
}
