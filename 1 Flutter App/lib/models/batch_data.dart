import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/batch.dart';

class BatchData extends ChangeNotifier {
  List<Batch> _batch = [
    Batch(
      batchNo: '01232323',
      productType: 'Product Type',
      startDate: '01/07/2021 16:04:45',
      endDate: '03/07/2021 16:04:45',
    ),
    Batch(
      batchNo: '01232324',
      productType: 'Product Type',
      startDate: '01/07/2021 16:04:45',
      endDate: '03/07/2021 16:04:45',
    ),
    Batch(
      batchNo: '01232325',
      productType: 'Product Type',
      startDate: '01/07/2021 16:04:45',
      endDate: '03/07/2021 16:04:45',
    ),
    Batch(
      batchNo: '01232326',
      productType: 'Product Type',
      startDate: '01/07/2021 16:04:45',
      endDate: '03/07/2021 16:04:45',
    ),
  ];

  UnmodifiableListView<Batch> get batch {
    return UnmodifiableListView(_batch);
  }

  int get batchCount {
    return _batch.length;
  }
}
