class Batch {
  final String batchNo;
  final String productType;
  final String startDate;
  final String endDate;

  Batch({
    this.batchNo = '',
    this.productType = '',
    this.startDate = '',
    this.endDate = '',
  });
}
