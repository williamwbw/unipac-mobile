import 'package:flutter/foundation.dart';
import 'dart:collection';
import 'package:unipacapp/models/event.dart';

class EventData extends ChangeNotifier {
  List<Event> _event = [
    Event(
      eventSummary: 'Event Summary Here',
      date: '03/07/2021 16:04:45',
    ),
  ];

  UnmodifiableListView<Event> get event {
    return UnmodifiableListView(_event);
  }

  int get eventCount {
    return _event.length;
  }
}
