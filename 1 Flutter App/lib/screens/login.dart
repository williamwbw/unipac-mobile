import 'package:flutter/material.dart';
import 'package:unipacapp/screens/home.dart';
import 'package:unipacapp/components/text_field.dart';
import 'package:unipacapp/utilities/constants.dart';

class LoginPage extends StatefulWidget {
  static const String id = 'login_screen';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 25),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: <Color>[
                    Theme.of(context).accentColor,
                    Color(0xffffffff),
                  ]),
            ),
            child: Column(
              children: [
                Expanded(
                  flex: 3,
                  child: Image.asset(
                    'images/QMAX-Logo.png',
                    width: 180,
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: SingleChildScrollView(
                    child: Stack(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 12.0,
                              ),
                              InputTextField(
                                label: 'Enter your Email',
                              ),
                              SizedBox(
                                height: 19.0,
                              ),
                              InputTextField(
                                label: 'Enter your Password',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          print('Login the system');
                          Navigator.pushNamed(context, HomePage.id);
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).primaryColor,
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 15.0),
                          child: Text(
                            'Login',
                            style: TextStyle(
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: TextButton(
                          onPressed: () {
                            showModalBottomSheet(
                                isScrollControlled: true,
                                context: context,
                                builder: (context) {
                                  return Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: <Widget>[
                                        Center(
                                          child: Text(
                                            'Forgot Password'.toUpperCase(),
                                            style: TextStyle(
                                              color: kColorPrimary,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 30.0,
                                        ),
                                        InputTextField(
                                          label: 'Enter your Email',
                                        ),
                                        SizedBox(
                                          height: 20.0,
                                        ),
                                        InputTextField(
                                          label: 'Confirm Email',
                                        ),
                                        SizedBox(
                                          height: 40.0,
                                        ),
                                        ElevatedButton(
                                          onPressed: () {
                                            print('Login the system');
                                            Navigator.pushNamed(
                                                context, HomePage.id);
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all<
                                                    Color>(
                                              Theme.of(context).primaryColor,
                                            ),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 15.0),
                                            child: Text(
                                              'Continue',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: MediaQuery.of(context)
                                                  .viewInsets
                                                  .bottom),
                                        ),
                                      ],
                                    ),
                                  );
                                });
                          },
                          style: TextButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                          ),
                          child: Text(
                            'Forgot Password ?',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
