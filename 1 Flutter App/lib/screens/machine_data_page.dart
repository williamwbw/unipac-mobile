import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/header.dart';
import 'package:unipacapp/components/outlined_button.dart';
import 'package:unipacapp/components/details_column.dart';
import 'package:unipacapp/components/GradientBackground.dart';
import 'package:unipacapp/components/test_distribution.dart';
import 'package:unipacapp/components/machine_details.dart';
import 'package:unipacapp/components/production_details.dart';
import 'package:unipacapp/components/status_details.dart';
import 'package:unipacapp/components/graph_view.dart';
import 'package:unipacapp/components/details_report.dart';

class MachineDataPage extends StatefulWidget {
  static const String id = 'machine_data_page';

  @override
  _MachineDataPageState createState() => _MachineDataPageState();
}

class _MachineDataPageState extends State<MachineDataPage> {
  Widget sectionSelected = MachineDetails();
  bool graph = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            GradientBg(),
            Container(
              child: Container(
                margin: EdgeInsets.only(bottom: 25),
                constraints: BoxConstraints(maxHeight: 800),
                child: SingleChildScrollView(
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          Container(
                            width: double.infinity,
                            padding: EdgeInsets.only(
                                top: 15, left: 10, right: 10, bottom: 13),
                            child: Column(
                              children: [
                                Header(
                                  label: 'Machine QME2344',
                                ),
                                SingleChildScrollView(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: WhiteOutlinedButton(
                                          label: 'Machine',
                                          fontSize: 10,
                                          onSectionSelect: () {
                                            setState(() {
                                              sectionSelected =
                                                  MachineDetails();
                                              graph = false;
                                            });
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Expanded(
                                        child: WhiteOutlinedButton(
                                          label: 'Production',
                                          fontSize: 10,
                                          onSectionSelect: () {
                                            setState(() {
                                              sectionSelected =
                                                  ProductionDetails();
                                              graph = false;
                                            });
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Expanded(
                                        child: WhiteOutlinedButton(
                                          label: 'Status',
                                          fontSize: 10,
                                          onSectionSelect: () {
                                            setState(() {
                                              sectionSelected = StatusDetails();
                                              graph = false;
                                            });
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Expanded(
                                        child: WhiteOutlinedButton(
                                          label: 'Graph',
                                          fontSize: 10,
                                          onSectionSelect: () {
                                            setState(() {
                                              sectionSelected =
                                                  Graph(view: 'machine');
                                              graph = true;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: (!graph)
                                ? EdgeInsets.symmetric(
                                    vertical: 25, horizontal: 25)
                                : EdgeInsets.symmetric(vertical: 0),
                            color:
                                (!graph) ? kBackgroundGrey : Colors.transparent,
                            child: (!graph)
                                ? Column(
                                    children: [
                                      sectionSelected,
                                      Divider(
                                        color: kColorGrey,
                                        height: 10,
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Center(
                                        child: DetailsColumn(
                                          label: 'Machine Calibration Due Date',
                                          value: '01/01/2021',
                                          alignment: CrossAxisAlignment.center,
                                        ),
                                      )
                                    ],
                                  )
                                : Graph(view: 'machine'),
                          ),
                          Container(
                            constraints: BoxConstraints(
                                minHeight:
                                    MediaQuery.of(context).size.height * .34),
                            color: (!graph) ? Colors.white : Colors.transparent,
                            child: Center(
                              child: (!graph)
                                  ? TestDistribution()
                                  : DetailsReport(),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
