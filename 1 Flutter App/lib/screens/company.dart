import 'package:flutter/material.dart';
import 'package:unipacapp/components/HeaderText.dart';
import 'package:unipacapp/components/ListScroll.dart';
import 'package:unipacapp/components/ListRow.dart';
import 'package:unipacapp/components/outlined_button.dart';
import 'package:unipacapp/components/GradientBackground.dart';
import 'package:unipacapp/components/round_number_container.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/models/machines_data.dart';
import 'package:unipacapp/components/header.dart';
import 'package:unipacapp/components/graph_view.dart';
import 'package:unipacapp/screens/operator.dart';
import 'package:unipacapp/screens/product.dart';

class CompanyPage extends StatelessWidget {
  static const String id = 'company_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            GradientBg(),
            Container(
              child: Center(
                child: Container(
                  margin: EdgeInsets.only(bottom: 25),
                  constraints: BoxConstraints(maxHeight: 800),
                  child: SingleChildScrollView(
                    child: Stack(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: double.infinity,
                              padding:
                                  EdgeInsets.only(top: 15, left: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Header(label: 'Unipac-HQ'),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      WhiteOutlinedButton(
                                        label: 'Operator  >',
                                        padding: 15,
                                        onSectionSelect: () {
                                          Navigator.pushNamed(
                                              context, OperatorPage.id);
                                        },
                                      ),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      WhiteOutlinedButton(
                                        label: 'Product  >',
                                        padding: 15,
                                        onSectionSelect: () {
                                          Navigator.pushNamed(
                                              context, ProductPage.id);
                                        },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Graph(view: 'company'),
                            Container(
                              padding: kPaddingHorizontal,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  HeaderText(
                                    label: 'Machines',
                                    padding: 0,
                                  ),
                                  Row(
                                    children: [
                                      RoundNumber(
                                        label: MachineData()
                                            .machineActive
                                            .toString(),
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      RoundNumber(
                                        label:
                                            MachineData().machineOff.toString(),
                                        color: kColorOff,
                                      ),
                                      RoundNumber(
                                        label: MachineData()
                                            .machineStandby
                                            .toString(),
                                        color: kColorStandby,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 8),
                                        child: Text('|'),
                                      ),
                                      RoundNumber(
                                        label: MachineData()
                                            .machineCount
                                            .toString(),
                                        color: kColorGrey,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            ListScroll(viewList: 'machine'),
                            HeaderText(label: 'Calibration'),
                            ListRow(viewList: 'calibration'),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
