import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/ListViewPage.dart';

class ProductPage extends StatelessWidget {
  static const String id = 'product_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kColorPrimary,
        title: Text(
          'Product Setting'.toUpperCase(),
        ),
      ),
      body: SafeArea(
        child: ListViewPage(
          index: 13,
          headerText: 'Product Title',
          description: 'Product Type',
          rightLabel: '01/07/2021 16:04:45',
          product: true,
        ),
      ),
    );
  }
}
