import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'dart:async';
import 'package:unipacapp/models/Services.dart';
import 'package:unipacapp/models/User.dart';

class MachineListView extends StatefulWidget {
  @override
  _MachineListViewState createState() => _MachineListViewState();
}

class _MachineListViewState extends State<MachineListView> {
  @override
  List<Machine> machines = [];
  List<Machine> filteredMachines = [];
  void initState() {
    super.initState();
    Services.getMachines().then((usersFromServer) {
      setState(() {
        machines = usersFromServer;
        filteredMachines = machines;
      });
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kColorPrimary,
        title: TextField(
          decoration: InputDecoration(
            hintText: 'Search Machine',
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
            hintStyle: TextStyle(color: Colors.blueGrey),
            fillColor: Colors.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.white, width: 1.0),
            ),
          ),
          onChanged: (string) {
            setState(() {
              filteredMachines = machines
                  .where((u) => (u.serialNum
                          .toLowerCase()
                          .contains(string.toLowerCase()) ||
                      u.companyName
                          .toLowerCase()
                          .contains(string.toLowerCase())))
                  .toList();
            });
          },
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: kPaddingHorizontal,
              margin: EdgeInsets.symmetric(vertical: 25),
              child: ListView.builder(
                itemCount: filteredMachines.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  final machineUni = filteredMachines[index];
                  return Container(
                    height: 60,
                    margin: EdgeInsets.only(bottom: 15),
                    decoration: BoxDecoration(
                      color: kBackgroundGrey,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: kPaddingHorizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(machineUni.serialNum, style: kTextBold),
                              Text(
                                machineUni.companyName,
                                style: kTextDescription,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
