import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/ListViewPage.dart';

class BatchPage extends StatefulWidget {
  static const String id = 'batch_page';
  @override
  _BatchPageState createState() => _BatchPageState();
}

class _BatchPageState extends State<BatchPage> {
//  List<Batch> batches = [];
//  List<Batch> filteredBatch = [];
//  void initState() {
//    super.initState();
//    Services.getBatch().then((usersFromServer) {
//      setState(() {
//        batches = usersFromServer;
//        filteredBatch = batches;
//      });
//    });
//  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kColorPrimary,
        title: TextField(
          decoration: InputDecoration(
            hintText: 'Search Batch',
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
            hintStyle: TextStyle(color: Colors.blueGrey),
            fillColor: Colors.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Colors.white, width: 1.0),
            ),
          ),
          onChanged: (string) {
//            setState(() {
//              filteredBatch = batches
//                  .where((u) => (u.serialNum
//                  .toLowerCase()
//                  .contains(string.toLowerCase()) ||
//                  u.companyName
//                      .toLowerCase()
//                      .contains(string.toLowerCase())))
//                  .toList();
//            });
          },
        ),
      ),
      body: SafeArea(
        child: ListViewPage(
          index: 13,
          headerText: 'Batch No',
          description: 'Product Type',
          rightLabel: '01/07/2021 16:04:45',
        ),
      ),
    );
  }
}
