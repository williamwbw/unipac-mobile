import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:unipacapp/components/HeaderText.dart';
import 'package:unipacapp/components/ListScroll.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/screens/machine_list_view.dart';

class HomePage extends StatelessWidget {
  static const String id = 'home_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      decoration:
                          BoxDecoration(color: Theme.of(context).primaryColor),
                      width: double.infinity,
                      height: 89,
                      margin: EdgeInsets.only(bottom: 25),
                    ),
                    Expanded(
                      child: Center(
                        child: Container(
                          margin: EdgeInsets.only(bottom: 25),
                          constraints: BoxConstraints(maxHeight: 800),
                          child: SingleChildScrollView(
                            child: Stack(
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    HeaderText(
                                      label: 'Companies',
                                    ),
                                    ListScroll(viewList: 'company'),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      padding: kPaddingHorizontal,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          HeaderText(
                                            label: 'Machines',
                                            padding: 0,
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (content) =>
                                                      MachineListView(),
                                                ),
                                              );
                                            },
                                            child: Text('View More',
                                                style: kTextUnderline),
                                          )
                                        ],
                                      ),
                                    ),
                                    ListScroll(viewList: 'machine-home'),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 55, left: 20, right: 20),
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    buildFloatingSearchBar(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget buildFloatingSearchBar() {
//  final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

  return FloatingSearchBar(
    hint: 'Search Company',
    scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
    transitionDuration: const Duration(milliseconds: 800),
    transitionCurve: Curves.easeInOut,
    physics: const BouncingScrollPhysics(),
    axisAlignment: 0.0,
    openAxisAlignment: 0.0,
    width: 600,
    debounceDelay: const Duration(milliseconds: 500),
    onQueryChanged: (query) {
      // Call your model, bloc, controller here.
    },
    // Specify a custom transition to be used for
    // animating between opened and closed stated.
    transition: CircularFloatingSearchBarTransition(),
    actions: [
      FloatingSearchBarAction.searchToClear(
        showIfClosed: true,
      ),
    ],
    builder: (context, transition) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Material(
          color: Colors.white,
          elevation: 4.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: Colors.accents.map((color) {
              return Container(height: 50, color: color);
            }).toList(),
          ),
        ),
      );
    },
  );
}
