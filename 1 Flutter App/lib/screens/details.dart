import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/batch_info_details.dart';
import 'package:unipacapp/components/product_setting_details.dart';

class DetailsPage extends StatelessWidget {
  static const String id = 'details_page';

  DetailsPage({
    this.product = false,
  });

  final bool product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kColorPrimary,
        title: (!product)
            ? Text(
                'Batch Info Details'.toUpperCase(),
              )
            : Text(
                'Product Setting Details'.toUpperCase(),
              ),
      ),
      body: SafeArea(
        child: ConstrainedBox(
          constraints:
              BoxConstraints.expand(width: MediaQuery.of(context).size.width),
          child: SingleChildScrollView(
            child: Container(
              child: (!product) ? BatchInfoDetails() : ProductSettingDetails(),
            ),
          ),
        ),
      ),
    );
  }
}
