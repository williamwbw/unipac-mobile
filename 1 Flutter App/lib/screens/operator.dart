import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/ListViewPage.dart';

class OperatorPage extends StatelessWidget {
  static const String id = 'operator_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: kColorPrimary,
        title: Text(
          'Operator Data'.toUpperCase(),
        ),
      ),
      body: SafeArea(
        child: ListViewPage(
          index: 13,
          headerText: 'Operator Name',
          description: 'Operator Email',
          rightLabel: 'ID : ' + '123344',
          details: true,
        ),
      ),
    );
  }
}
