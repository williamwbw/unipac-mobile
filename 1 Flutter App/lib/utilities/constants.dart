import 'package:flutter/material.dart';

const kColorPrimary = Color(0xff3E8D80);
const kColorOff = Color(0xffF1435B);
const kColorStandby = Color(0xffF9B115);
const kColorGrey = Color(0xff8F9BB3);
const kColorBlue = Color(0xff5B78F1);
const kBgColorPrimary = Color(0xffCEEBCF);
const kBgColorStandby = Color(0xffFFF2D5);
const kBackgroundGrey = Color(0xffF7F9FC);
const kBorderColor = Color(0xffCEDEEB);
const kPaddingHorizontal = EdgeInsets.symmetric(horizontal: 25);
const kTextUnderline = TextStyle(
  color: kColorPrimary,
  decoration: TextDecoration.underline,
);
const kTextBold = TextStyle(
  fontWeight: FontWeight.w700,
  fontSize: 15,
);
const kTextDescription = TextStyle(fontWeight: FontWeight.w400);
const kTextSmall = TextStyle(
  fontWeight: FontWeight.w400,
  fontSize: 12,
);
const kTextBoldPrimary = TextStyle(
  fontWeight: FontWeight.w500,
  color: kColorPrimary,
);
const kTextBoldOff = TextStyle(
  fontWeight: FontWeight.w500,
  color: kColorOff,
);
const kDropdownText = TextStyle(fontSize: 14, color: Colors.black);
