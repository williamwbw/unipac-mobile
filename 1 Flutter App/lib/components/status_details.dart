import 'package:flutter/material.dart';
import 'package:unipacapp/components/details_column.dart';

class StatusDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: '12V Voltmeter',
                value: 'A',
              ),
              DetailsColumn(
                label: '24V Voltmeter',
                value: 'Zone A',
              ),
              DetailsColumn(
                label: '12 Ammeter',
                value: 'Zone A',
              ),
              DetailsColumn(
                label: '24 Ammeter',
                value: 'Zone A',
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: 'Air Pressure',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'Tempreature',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'Low Pressure',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'E-stop',
                value: 'Operator',
              ),
            ],
          ),
        ),
      ],
    );
  }
}
