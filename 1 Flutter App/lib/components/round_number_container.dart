import 'package:flutter/material.dart';

class RoundNumber extends StatelessWidget {
  RoundNumber({required this.color, this.label = 'N', this.margin = 8});
  final Color color;
  final String label;
  final double margin;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: 25,
        height: 25,
        margin: EdgeInsets.only(left: margin),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Center(
            child: Text(
          label,
          style: TextStyle(color: Colors.white),
        )),
      ),
    );
  }
}
