import 'package:flutter/material.dart';
import 'package:unipacapp/models/calibration_data.dart';
import 'package:unipacapp/utilities/constants.dart';

class CalibrationList extends StatelessWidget {
  final CalibrationData calibration = CalibrationData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: calibration.calibrationCount,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final calibrations = calibration.calibration[index];

        return Container(
          height: 60,
          margin: EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            color: kBackgroundGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: kPaddingHorizontal,
          child: GestureDetector(
            onTap: () {
              print('clicked machine ' + calibrations.serialnumber);
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Machine S/N',
                        style: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 10),
                      ),
                      Text(
                        calibrations.serialnumber,
                        style: kTextBold,
                      )
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    calibrations.date,
                    style: kTextBoldOff,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
