import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/screens/details.dart';

class ListViewPage extends StatelessWidget {
  ListViewPage({
    this.index = 0,
    this.headerText = 'Header Text',
    this.rightLabel = 'Date',
    this.description = 'Description',
    this.product = false,
    this.details = false,
  });

  final int index;
  final String headerText;
  final String rightLabel;
  final String description;
  final bool product;
  final bool details;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingHorizontal,
      margin: EdgeInsets.symmetric(vertical: 25),
      child: ListView.builder(
        itemCount: index,
        shrinkWrap: true,
        itemBuilder: (context, index) {
//            final faults = fault.fault[index];
          return Container(
            height: 60,
            margin: EdgeInsets.only(bottom: 15),
            decoration: BoxDecoration(
              color: kBackgroundGrey,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: kPaddingHorizontal,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                if (product) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (content) => DetailsPage(
                        product: true,
                      ),
                    ),
                  );
                } else if (details) {
                  print('No Details Available');
                } else {
                  Navigator.pushNamed(context, DetailsPage.id);
                }
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(headerText, style: kTextBold),
                        Text(
                          description,
                          style: kTextDescription,
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: 100,
                    child: Text(
                      rightLabel,
                      textAlign: TextAlign.center,
                      style: kTextBoldPrimary,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
