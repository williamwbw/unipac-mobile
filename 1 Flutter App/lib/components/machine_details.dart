import 'package:flutter/material.dart';
import 'package:unipacapp/components/details_column.dart';

class MachineDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: 'Company',
                value: 'Company A',
              ),
              DetailsColumn(
                label: 'Location',
                value: 'Zone A',
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: 'Years of Manufacturing',
                value: '11/12/2021',
              ),
              DetailsColumn(
                label: 'Software Version',
                value: 'V4.5',
              ),
            ],
          ),
        ),
      ],
    );
  }
}
