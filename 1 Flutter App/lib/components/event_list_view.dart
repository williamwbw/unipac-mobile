import 'package:flutter/material.dart';
import 'package:unipacapp/models/event_data.dart';
import 'package:unipacapp/utilities/constants.dart';

class EventList extends StatelessWidget {
  final EventData event = EventData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: event.eventCount,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final events = event.event[index];

        return Container(
          height: 60,
          margin: EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            color: kBackgroundGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: kPaddingHorizontal,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      events.eventSummary,
                      style: kTextDescription,
                    )
                  ],
                ),
              ),
              Container(
                width: 100,
                child: Text(events.date,
                    textAlign: TextAlign.center, style: kTextBoldPrimary),
              ),
            ],
          ),
        );
      },
    );
  }
}
