import 'package:flutter/material.dart';
import 'package:unipacapp/models/companies_data.dart';
import 'package:unipacapp/screens/company.dart';

class CompanyList extends StatelessWidget {
  final CompanyData company = CompanyData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: company.taskCount,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final task = company.tasks[index];
        return Container(
          width: 170,
          margin: EdgeInsets.only(bottom: 15, right: 15),
          decoration: BoxDecoration(
            color: index.isOdd
                ? Theme.of(context).primaryColor
                : Theme.of(context).accentColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              print('clicked company ' + task.name);
              Navigator.pushNamed(context, CompanyPage.id);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    text: TextSpan(
                      text: task.name.toUpperCase(),
                      style: TextStyle(
                        color:
                            index.isOdd ? Color(0xffFFFFFF) : Color(0xff000000),
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Text(
                  task.email,
                  style: TextStyle(
                    color: index.isOdd ? Color(0xffFFFFFF) : Color(0xff000000),
                  ),
                ),
                Flexible(
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 4,
                    text: TextSpan(
                      text: task.address,
                      style: TextStyle(
                        color:
                            index.isOdd ? Color(0xffFFFFFF) : Color(0xff000000),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
