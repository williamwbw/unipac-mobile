import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/details_column.dart';

class ProductSettingDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.symmetric(
        inside: BorderSide(
          width: 1,
          color: kBorderColor,
        ),
        outside: BorderSide(
          width: 1,
          color: kBorderColor,
        ),
      ),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(
          children: [
            DetailsColumn(
              label: 'Product Type',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Air Eject Good',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Product Size',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Air Eject Retest',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Product Register Time',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Start Test',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Inflate Pressure',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Test For',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Inflate 1',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Pass Code',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Inflate 2',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Glove Type',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Reject Assist',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'State Glove Type',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Glove illumination',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: '',
              value: '',
              bottom: false,
            ),
          ],
        ),
      ],
    );
  }
}
