import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';

class DropdownItem {
  DropdownItem({required this.name, required this.value});
  final String name;
  final String value;
}

class ExactTimeDropdown extends StatefulWidget {
  ExactTimeDropdown({required this.exactTime});
  final String exactTime;
  @override
  _ExactTimeDropdownState createState() => _ExactTimeDropdownState();
}

class _ExactTimeDropdownState extends State<ExactTimeDropdown> {
  @override
  void initState() {
    print(widget.exactTime);
    super.initState();
  }

  List<DropdownItem> dropdownExactTimeItems = <DropdownItem>[];
  Widget build(BuildContext context) {
    String dropdownExactTimeLabel = '';
    if (widget.exactTime == 'year') {
      dropdownExactTimeLabel = '2021';
      dropdownExactTimeItems = [
        DropdownItem(name: '2021', value: '2021'),
        DropdownItem(name: '2020', value: '2020'),
      ];
    } else if (widget.exactTime == 'month') {
      dropdownExactTimeLabel = 'jan';
      dropdownExactTimeItems = [
        DropdownItem(name: 'Jan', value: 'jan'),
        DropdownItem(name: 'Feb', value: 'feb'),
        DropdownItem(name: 'March', value: 'mar'),
        DropdownItem(name: 'April', value: 'apr'),
        DropdownItem(name: 'May', value: 'may'),
        DropdownItem(name: 'Jun', value: 'jun'),
        DropdownItem(name: 'July', value: 'july'),
        DropdownItem(name: 'Aug', value: 'aug'),
        DropdownItem(name: 'Sept', value: 'sept'),
        DropdownItem(name: 'October', value: 'jun'),
        DropdownItem(name: 'November', value: 'nov'),
        DropdownItem(name: 'December', value: 'dec'),
      ];
    } else if (widget.exactTime == 'threemonth') {
      dropdownExactTimeLabel = 'janmarch';
      dropdownExactTimeItems = [
        DropdownItem(name: 'Jan - March', value: 'janmarch'),
        DropdownItem(name: 'April - Jun', value: 'aprjun'),
        DropdownItem(name: 'July - Sept', value: 'julysept'),
        DropdownItem(name: 'October - December', value: 'octdec'),
      ];
    }
    return DropdownButton<String>(
      style: kDropdownText,
      value: dropdownExactTimeLabel,
      items: dropdownExactTimeItems.map((DropdownItem dropdownItems) {
        return DropdownMenuItem(
          value: dropdownItems.value,
          child: Text(dropdownItems.name),
        );
      }).toList(),
      onChanged: (value) {
        setState(() {
          dropdownExactTimeLabel = value.toString();
          print(value);
        });
      },
    );
  }
}
