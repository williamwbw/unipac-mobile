import 'package:flutter/material.dart';
import 'package:unipacapp/models/batch_data.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/screens/details.dart';

class BatchList extends StatelessWidget {
  final BatchData batch = BatchData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: batch.batchCount,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final batches = batch.batch[index];

        return Container(
          height: 60,
          margin: EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            color: kBackgroundGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: kPaddingHorizontal,
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              Navigator.pushNamed(context, DetailsPage.id);
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(batches.batchNo, style: kTextBold),
                      Text(batches.productType, style: kTextDescription)
                    ],
                  ),
                ),
                Container(
                  width: 180,
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          batches.startDate,
                          textAlign: TextAlign.center,
                          style: kTextBoldPrimary,
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Text(
                          batches.endDate,
                          textAlign: TextAlign.center,
                          style: kTextBoldPrimary,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
