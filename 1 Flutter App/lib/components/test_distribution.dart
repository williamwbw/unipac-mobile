import 'package:flutter/material.dart';
import 'package:unipacapp/components/HeaderText.dart';
import 'package:unipacapp/components/digits_container.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/details_column.dart';

class TestDistribution extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        HeaderText(
          label: 'Test Distribution',
        ),
        Container(
          padding: kPaddingHorizontal,
          child: Row(
            children: [
              DigitContainer(
                label: 'passed',
                number: 0,
                percentage: 0.01,
                color: kColorPrimary,
              ),
              DigitContainer(
                  label: 'failed',
                  number: 0,
                  percentage: 0.01,
                  color: kColorOff),
              DigitContainer(
                label: 'retest',
                number: 0,
                percentage: 0.01,
                color: kColorStandby,
              ),
              DigitContainer(
                  label: 'reject',
                  number: 0,
                  percentage: 0.01,
                  color: kColorGrey),
            ],
          ),
        ),
        Container(
          padding: kPaddingHorizontal,
          child: Row(
            children: [
              DigitContainer(
                label: 'Total Glove',
                number: 0,
                color: kColorBlue,
              ),
              DigitContainer(
                label: 'No Glove',
                number: 0,
                color: kColorBlue,
              ),
            ],
          ),
        ),
        Container(
          padding: kPaddingHorizontal,
          margin: EdgeInsets.only(top: 15),
          child: Row(
            children: [
              Expanded(
                child: DetailsColumn(
                  label: 'Rate',
                  value: '0',
                ),
              ),
              Expanded(
                child: DetailsColumn(
                  label: 'Round .T',
                  value: '0',
                ),
              ),
              Expanded(
                child: DetailsColumn(
                  label: 'Interval',
                  value: '0',
                ),
              ),
              Expanded(
                child: DetailsColumn(
                  label: 'Run Time',
                  value: '0',
                ),
              ),
              Expanded(
                child: DetailsColumn(
                  label: 'Efficientcy',
                  value: '0',
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
