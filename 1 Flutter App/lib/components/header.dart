import 'package:flutter/material.dart';
import 'package:unipacapp/components/HeaderText.dart';

class Header extends StatelessWidget {
  Header({this.label = 'header'});
  final String label;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          flex: 1,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.transparent,
                elevation: 0,
                textStyle: TextStyle(fontSize: 14)),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
            ),
          ),
        ),
        Expanded(
          flex: 5,
          child: HeaderText(
            label: label,
            alignment: Alignment.center,
            colour: Colors.white,
            size: 20,
            padding: 0,
          ),
        ),
        Expanded(child: Container())
      ],
    );
  }
}
