import 'package:flutter/material.dart';
import 'package:unipacapp/components/details_column.dart';

class ProductionDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: 'Product Type',
                value: 'A',
              ),
              DetailsColumn(
                label: 'Product Size',
                value: 'Zone A',
              ),
              DetailsColumn(
                label: 'Batch Info',
                value: 'Zone A',
              ),
              DetailsColumn(
                label: 'Inspector',
                value: 'Zone A',
              ),
            ],
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DetailsColumn(
                label: 'Operator 1',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'Operator 2',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'Operator 3',
                value: 'Operator',
              ),
              DetailsColumn(
                label: 'Operator 4',
                value: 'Operator',
              ),
            ],
          ),
        ),
      ],
    );
  }
}
