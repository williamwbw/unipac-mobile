import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/ListRow.dart';
import 'package:unipacapp/screens/batch.dart';

class DetailsReport extends StatefulWidget {
  @override
  _DetailsReportState createState() => _DetailsReportState();
}

class Item {
  const Item(this.name, this.value);
  final String name;
  final String value;
}

class _DetailsReportState extends State<DetailsReport> {
  Widget sectionSelected = ListRow(
    viewList: 'batchinfo',
  );
  bool batch = true;
  String dropdownLabel = 'batchinfo';
  List<Item> dropdownItems = <Item>[
    Item('Batch Info', 'batchinfo'),
    Item('Event Log', 'eventlog'),
    Item('Fault Report', 'faultreport'),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: kPaddingHorizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DropdownButton<String>(
                  style: kDropdownText,
                  value: dropdownLabel,
                  items: dropdownItems.map((Item dropdownItems) {
                    return DropdownMenuItem(
                      value: dropdownItems.value,
                      child: Text(dropdownItems.name),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      sectionSelected = ListRow(
                        viewList: value.toString(),
                      );
                      dropdownLabel = value.toString();
                      if (value == 'batchinfo') {
                        batch = true;
                      } else {
                        batch = false;
                      }
                    });
                  },
                ),
                Container(
                  child: (batch)
                      ? TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, BatchPage.id);
                          },
                          child: Text(
                            'View More',
                            style: kTextUnderline,
                          ),
                        )
                      : Container(),
                ),
              ],
            ),
          ),
          sectionSelected,
        ],
      ),
    );
  }
}
