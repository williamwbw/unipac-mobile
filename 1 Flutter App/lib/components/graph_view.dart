import 'package:flutter/material.dart';
import 'package:unipacapp/components/sensor_data_buttons.dart';
import 'package:unipacapp/components/graph_exact_time_dropdown.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class Graph extends StatefulWidget {
  Graph({required this.view});
  final String view;
  @override
  _GraphState createState() => _GraphState();
}

class ProductionYield {
  final String status;
  final int yield;
  final charts.Color color;

  ProductionYield(this.status, this.yield, Color color)
      : this.color = charts.Color(
          r: color.red,
          g: color.green,
          b: color.blue,
          a: color.alpha,
        );
}

class _GraphState extends State<Graph> {
  @override
  void initState() {
    print(widget.view);
    super.initState();
  }

  String graphView = "productionyield";
  String dropdownLabel = 'productionyield';
  String dropdownTimeLabel = 'year';
  List<DropdownItem> dropdownItems = <DropdownItem>[];
  List<DropdownItem> dropdownTimeItems = <DropdownItem>[
    DropdownItem(name: 'Year', value: 'year'),
    DropdownItem(name: 'Month', value: 'month'),
    DropdownItem(name: 'Three Month', value: 'threemonth'),
  ];

  Widget build(BuildContext context) {
//    check come in as graph is company page or machine page
    if (widget.view == 'company') {
      dropdownItems = [
        DropdownItem(name: 'Production Yield', value: 'productionyield'),
        DropdownItem(name: 'Production Volume', value: 'productionvolume'),
      ];
    } else if (widget.view == 'machine') {
      dropdownItems = [
        DropdownItem(name: 'Production Yield', value: 'productionyield'),
        DropdownItem(name: 'Production Volume', value: 'productionvolume'),
        DropdownItem(name: 'Sensor Data', value: 'sensordata'),
      ];
    }
    //Graph Details
    var data = [
      ProductionYield('Pass', 12, kColorPrimary),
      ProductionYield('Fail', 42, kColorOff),
      ProductionYield('Retest', 10, kColorStandby),
      ProductionYield('Reject', 10, kColorGrey),
    ];

    var series = [
      charts.Series(
        domainFn: (ProductionYield yield, _) => yield.status,
        measureFn: (ProductionYield yield, _) => yield.yield,
        colorFn: (ProductionYield yield, _) => yield.color,
        id: 'Yield',
        data: data,
      ),
    ];
    var chart = charts.BarChart(
      series,
      animate: true,
    );
    var chartWidget = Container(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.all(10),
      child: SizedBox(
        height: 250.0,
        child: chart,
      ),
    );
    return Container(
      padding: kPaddingHorizontal,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              DropdownButton<String>(
                style: kDropdownText,
                value: dropdownLabel,
                items: dropdownItems.map((DropdownItem dropdownItems) {
                  return DropdownMenuItem(
                    value: dropdownItems.value,
                    child: Text(dropdownItems.name),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    dropdownLabel = value.toString();
                    graphView = value.toString();
                  });
                },
              ),
              (graphView != 'sensordata')
                  ? DropdownButton<String>(
                      style: kDropdownText,
                      value: dropdownTimeLabel,
                      items:
                          dropdownTimeItems.map((DropdownItem dropdownItems) {
                        return DropdownMenuItem(
                          value: dropdownItems.value,
                          child: Text(dropdownItems.name),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          dropdownTimeLabel = value.toString();
                        });
                      },
                    )
                  : Container(),
            ],
          ),
          chartWidget,
          Container(
            child: (graphView == "productionyield")
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ExactTimeDropdown(exactTime: dropdownTimeLabel),
                      ElevatedButton(
                        onPressed: () {
//                          change graph here
                        },
                        style: ElevatedButton.styleFrom(
                          primary: kColorPrimary,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          minimumSize: Size(10, 25),
                        ),
                        child: Text(
                          'No Glove',
                          style: kTextSmall,
                        ),
                      )
                    ],
                  )
                : (graphView == 'productionvolume')
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ExactTimeDropdown(exactTime: dropdownTimeLabel),
                        ],
                      )
                    : (graphView == 'sensordata')
                        ? SensorButtons()
                        : Container(),
          ),
          Divider(
            color: kColorGrey,
            height: 25,
          ),
        ],
      ),
    );
  }
}
