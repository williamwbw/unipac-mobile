import 'package:flutter/material.dart';
import 'package:unipacapp/components/calibration_list_view.dart';
import 'package:unipacapp/components/batch_list_view.dart';
import 'package:unipacapp/components/event_list_view.dart';
import 'package:unipacapp/components/fault_list_view.dart';

class ListRow extends StatelessWidget {
  final String viewList;

  ListRow({this.viewList = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 25, right: 25),
      child: Container(
          constraints: viewList == 'calibration' ||
                  viewList == 'batchinfo' ||
                  viewList == 'eventlog' ||
                  viewList == 'faultreport'
              ? BoxConstraints(minHeight: 300, maxHeight: 300)
              : BoxConstraints(minHeight: 300, maxHeight: 300),
          child: viewList == 'calibration'
              ? CalibrationList()
              : viewList == 'batchinfo'
                  ? BatchList()
                  : viewList == 'eventlog'
                      ? EventList()
                      : viewList == 'faultreport'
                          ? FaultList()
                          : null
//            ? CompanyList()
//            : MachineList(),
          ),
    );
  }
}
