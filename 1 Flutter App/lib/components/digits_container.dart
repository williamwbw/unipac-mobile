import 'package:flutter/material.dart';

class DigitContainer extends StatelessWidget {
  DigitContainer({
    required this.label,
    required this.number,
    this.percentage = 0,
    required this.color,
  });

  final String label;
  final int number;
  final double percentage;
  final Color color;

  @override
  Widget build(BuildContext context) {
    var percentageContainer;

    if (percentage > 0) {
      percentageContainer = Container(
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            Text(
              percentage.toString() + '%',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      );
    } else {
      percentageContainer = Container(
        height: 0,
      );
    }

    return Expanded(
      child: Container(
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          children: [
            Text(
              label.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              number.toString(),
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            percentageContainer,
          ],
        ),
      ),
    );
  }
}
