import 'package:flutter/material.dart';
import 'package:unipacapp/screens/home.dart';

class WhiteOutlinedButton extends StatelessWidget {
  WhiteOutlinedButton({
    this.label = 'Button Text',
    this.fontSize = 12,
    this.padding = 0,
    required this.onSectionSelect,
  });

  final String label;
  final double fontSize;
  final double padding;
  final Function onSectionSelect;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          elevation: 0,
          side: BorderSide(color: Colors.white),
          padding: EdgeInsets.symmetric(horizontal: padding),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
          )),
      onPressed: () {
        onSectionSelect();
      },
      child: Text(
        label.toUpperCase(),
        style: TextStyle(fontWeight: FontWeight.w400, fontSize: fontSize),
      ),
    );
  }
}
