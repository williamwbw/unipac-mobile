import 'dart:ffi';

import 'package:flutter/material.dart';

class HeaderText extends StatelessWidget {
  HeaderText({
    this.label = 'Input Header Here',
    this.alignment = Alignment.centerLeft,
    this.colour = Colors.black,
    this.size = 18,
    this.padding = 25,
  });

  final String label;
  final Alignment alignment;
  final Color colour;
  final double size;
  final double padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: padding),
      margin: EdgeInsets.only(top: 10, bottom: 15),
      child: Align(
        alignment: alignment,
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: size,
            color: colour,
          ),
        ),
      ),
    );
  }
}
