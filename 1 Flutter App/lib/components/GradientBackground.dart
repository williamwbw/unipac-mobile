import 'package:flutter/material.dart';

class GradientBg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[
              Theme.of(context).primaryColor,
              Color(0xffffffff),
            ]),
      ),
      width: double.infinity,
      height: 500,
      margin: EdgeInsets.only(bottom: 25),
    );
  }
}
