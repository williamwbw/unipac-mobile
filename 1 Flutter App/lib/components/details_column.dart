import 'package:flutter/material.dart';

class DetailsColumn extends StatelessWidget {
  DetailsColumn({
    required this.label,
    required this.value,
    this.alignment = CrossAxisAlignment.start,
    this.bottom = true,
  });

  final String label;
  final String value;
  final alignment;
  final bool bottom;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: (!bottom) ? EdgeInsets.all(16) : EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: alignment,
        children: [
          Text(
            label,
            style: TextStyle(fontSize: 12),
          ),
          Text(
            value,
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),
          ),
          Container(
            child: (bottom)
                ? SizedBox(
                    height: 15,
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
