import 'package:flutter/material.dart';
import 'package:unipacapp/models/machines_data.dart';
import 'package:unipacapp/utilities/constants.dart';

class MachineHomeList extends StatelessWidget {
  final MachineData machine = MachineData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: machine.machineCount,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final machines = machine.machines[index];

        return Container(
          width: 140,
          margin: EdgeInsets.only(bottom: 15, right: 15),
          decoration: BoxDecoration(
            color: kBackgroundGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Text(
                  machines.companyName.toUpperCase(),
                  style: kTextBoldPrimary,
                ),
              ),
              Text(
                "-\nMACHINE\n" + machines.serialNum,
                style: kTextBold,
              ),
            ],
          ),
        );
      },
    );
  }
}
