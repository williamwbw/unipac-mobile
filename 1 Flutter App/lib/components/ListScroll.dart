import 'package:flutter/material.dart';
import 'package:unipacapp/components/company_list_view.dart';
import 'package:unipacapp/components/machine_list_view.dart';
import 'package:unipacapp/components/machine_home_list_view.dart';
import 'package:unipacapp/utilities/constants.dart';

class ListScroll extends StatelessWidget {
  final String viewList;
  ListScroll({this.viewList = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingHorizontal,
      child: Container(
        constraints: viewList == 'machine'
            ? BoxConstraints(minHeight: 50, maxHeight: 130)
            : viewList == 'machine-home'
                ? BoxConstraints(minHeight: 50, maxHeight: 170)
                : BoxConstraints(minHeight: 50, maxHeight: 230),
        child: viewList == 'machine'
            ? MachineList()
            : viewList == 'machine-home'
                ? MachineHomeList()
                : viewList == 'company'
                    ? CompanyList()
                    : Container(),
      ),
    );
  }
}
