import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/models/fault_data.dart';

class FaultList extends StatelessWidget {
  final FaultData fault = FaultData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: fault.faultCount,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final faults = fault.fault[index];

        return Container(
          height: 60,
          margin: EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            color: kBackgroundGrey,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: kPaddingHorizontal,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      faults.faultSummary,
                      style: kTextBold,
                    ),
                    Text(
                      faults.faultLog,
                      style: kTextDescription,
                    )
                  ],
                ),
              ),
              Container(
                width: 100,
                child: Text(
                  faults.date,
                  textAlign: TextAlign.center,
                  style: kTextBoldPrimary,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
