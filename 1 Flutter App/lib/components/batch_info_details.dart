import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/components/details_column.dart';

class BatchInfoDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.symmetric(
        inside: BorderSide(
          width: 1,
          color: kBorderColor,
        ),
        outside: BorderSide(
          width: 1,
          color: kBorderColor,
        ),
      ),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(
          children: [
            DetailsColumn(
              label: 'Batch No',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Operator 2',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Product Type',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Operator 3',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Start Time',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Operator 4',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'End Time',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Inspector',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Product Size',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Passed',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Total Glove',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Retest',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Rate',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Failed',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Glove Type',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'Reject',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
        TableRow(
          children: [
            DetailsColumn(
              label: 'Operator 1',
              value: 'value',
              bottom: false,
            ),
            DetailsColumn(
              label: 'No. Glove',
              value: 'value',
              bottom: false,
            ),
          ],
        ),
      ],
    );
  }
}
