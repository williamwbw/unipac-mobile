import 'package:flutter/material.dart';
import 'package:unipacapp/utilities/constants.dart';

class SensorButtons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          onPressed: () {
            //                          change graph here
          },
          style: ElevatedButton.styleFrom(
            primary: kColorPrimary,
            padding: EdgeInsets.symmetric(horizontal: 10),
            minimumSize: Size(80, 25),
          ),
          child: Text(
            '12V',
            style: kTextSmall,
          ),
        ),
        SizedBox(
          width: 5,
        ),
        ElevatedButton(
          onPressed: () {
            //                          change graph here
          },
          style: ElevatedButton.styleFrom(
            primary: kColorPrimary,
            padding: EdgeInsets.symmetric(horizontal: 10),
            minimumSize: Size(80, 25),
          ),
          child: Text(
            'Air Pressure',
            style: kTextSmall,
          ),
        ),
        SizedBox(
          width: 5,
        ),
        ElevatedButton(
          onPressed: () {
            //                          change graph here
          },
          style: ElevatedButton.styleFrom(
            primary: kColorPrimary,
            padding: EdgeInsets.symmetric(horizontal: 10),
            minimumSize: Size(80, 25),
          ),
          child: Text(
            'Temperature',
            style: kTextSmall,
          ),
        ),
      ],
    );
  }
}
