import 'package:flutter/material.dart';
import 'package:unipacapp/models/machines_data.dart';
import 'package:unipacapp/utilities/constants.dart';
import 'package:unipacapp/screens/machine_data_page.dart';

class MachineList extends StatelessWidget {
  final MachineData machine = MachineData();
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: machine.machineCount,
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        final machines = machine.machines[index];

        var checkStatusColor;
        if (machines.status == 'standby' || machines.status == 'Standby') {
          checkStatusColor = kColorStandby;
        } else if (machines.status == 'off' || machines.status == 'Off') {
          checkStatusColor = kColorOff;
        } else {
          checkStatusColor = kColorPrimary;
        }

        var checkBackgroundColor;
        if (machines.status == 'standby' || machines.status == 'Standby') {
          checkBackgroundColor = kBgColorStandby;
        } else if (machines.status == 'off' || machines.status == 'Off') {
          checkBackgroundColor = kBackgroundGrey;
        } else {
          checkBackgroundColor = kBgColorPrimary;
        }

        return Container(
          width: 140,
          margin: EdgeInsets.only(bottom: 15, right: 15),
          decoration: BoxDecoration(
            color: checkBackgroundColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              print('clicked machine ' + machines.serialNum);
              Navigator.pushNamed(context, MachineDataPage.id);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                      decoration: BoxDecoration(
                        color: checkStatusColor,
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Text(
                        machines.status.toUpperCase(),
                        style: TextStyle(
                          color: machines.status == 'standby'
                              ? Colors.black
                              : Colors.white,
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  "-\nMACHINE\n" + machines.serialNum,
                  style: kTextBold,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
